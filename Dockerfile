FROM oraclelinux:7-slim

COPY oracle-instantclient12.1-* /tmp/

RUN yum install -y /tmp/oracle-instantclient12.1-* && \
    rm -rf /var/cache/yum && \
    ldconfig

ENV PATH=$PATH:/usr/lib/oracle/12.1/client64/bin
ENV ORACLE_HOME=/usr/lib/oracle/12.1/client64
ENV LD_LIBRARY_PATH=$ORACLE_HOME/lib

CMD ["sqlplus", "-v"]